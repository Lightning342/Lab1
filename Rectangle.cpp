#include "Rectangle.h"





Rectangle::Rectangle(rectangle_t passedRect):
  width_(passedRect.width), height_(passedRect.height)
{
  pos_ = passedRect.pos;
}

double Rectangle::getArea() const
{
  return width_*height_;
}

rectangle_t Rectangle::getFrameRect() const
{
  rectangle_t rect_return;
  rect_return.pos = pos_;
  rect_return.width = width_;
  rect_return.height = height_;
  return rect_return;
}
