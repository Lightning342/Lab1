#include "Circle.h"
#define _USE_MATH_DEFINES
#include <math.h>



Circle::Circle(point_t passedPos, double passedRadius):
  rad_(passedRadius)
{
  pos_ = passedPos;
}

double Circle::Circle::getArea() const
{
  return M_PI*pow(rad_, 2);
}

rectangle_t Circle::Circle::getFrameRect() const
{
  rectangle_t rect_return = {rad_*2, rad_*2, pos_};
  return rect_return;
}


