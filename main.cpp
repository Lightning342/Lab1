#include "Base-types.h"
#include "Shape.h"
#include "Rectangle.h"
#include "Circle.h"
#include <iostream>

void const printArea(const Shape & figure)
{
  std::cout << figure.getArea() << std::endl;
}

int main()
{
  Rectangle rect_test({100, 100, 40, 20});
  printArea(rect_test);

  Circle circle_test({200, 200}, 10);
  printArea(circle_test);
  return 0;
}
